import React, {useContext} from 'react';
import {
  Button,
  Image,
  ImageBackground,
  ScrollView,
  Text,
  View,
} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {AppContext} from '../contexts/appContext';
import Back_img from '../images/download.jpeg';
export default function HomeScreen() {
  const {setAppName} = useContext(AppContext);

  function clickMe() {
    setAppName('Ase Note');
  }
  return (
    <View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Image source={Back_img} style={{width: '100%', height: 200}} />
        <Button onPress={clickMe} title="Click me" />
        <Text
          style={{
            marginTop: 20,
            fontWeight: 'bold',
            fontSize: 19,
            marginLeft: 10,
          }}>
          BIT 8th sem notes
        </Text>
        <View style={{marginVertical: 10}}>
          <ScrollView horizontal={true}>
            {[0, 1, 3, 4, 5].map((item, index) => {
              return (
                <View
                  key={index}
                  style={{
                    padding: 10,
                    backgroundColor: 'gray',
                    margin: 8,
                    borderRadius: 4,
                  }}>
                  <Text>Bit 1st sem </Text>
                  <Text>{item}</Text>
                  {/* <Image source={Back_img} style={{width: 100, height: 50}} /> */}
                </View>
              );
            })}
          </ScrollView>
        </View>
        <Text
          style={{
            marginTop: 20,
            fontWeight: 'bold',
            fontSize: 19,
            marginLeft: 10,
          }}>
          BE-CIVIL Syllabus(4 Years)
        </Text>
        <View style={{marginVertical: 10}}>
          <ScrollView horizontal={true}>
            {[0, 1, 3, 4, 5].map((item, index) => {
              return (
                <View
                  key={index}
                  style={{
                    padding: 10,
                    backgroundColor: 'gray',
                    margin: 8,
                    borderRadius: 4,
                  }}>
                  <Text>Bit 1st sem </Text>
                  <Text>{item}</Text>
                </View>
              );
            })}
          </ScrollView>
        </View>
        <Text
          style={{
            marginTop: 20,
            fontWeight: 'bold',
            fontSize: 19,
            marginLeft: 10,
          }}>
          BIT 1st sem notes
        </Text>
        <View style={{marginVertical: 10}}>
          <ScrollView horizontal={true}>
            {[0, 1, 3, 4, 5].map((item, index) => {
              return (
                <View
                  key={index}
                  style={{
                    padding: 10,
                    backgroundColor: 'gray',
                    margin: 8,
                    borderRadius: 4,
                  }}>
                  <Text>Bit 1st sem </Text>
                  <Text>{item}</Text>
                </View>
              );
            })}
          </ScrollView>
        </View>
      </ScrollView>
    </View>
  );
}
