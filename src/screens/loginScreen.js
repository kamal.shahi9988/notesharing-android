import React from 'react';
import {Button, Text, View} from 'react-native';
import {useNavigation, useRoute} from '@react-navigation/native';

const LoginScreen = () => {
  const {navigate, goBack} = useNavigation();
  const {params} = useRoute();
  return (
    <View>
      <Text>Hello {params.name}</Text>
      <Text>College is {params.college}</Text>
      <Button
        onPress={() => {
          goBack();
        }}
        title="Back to home"
      />
    </View>
  );
};

export default LoginScreen;
