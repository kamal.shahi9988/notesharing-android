import React, {useReducer, useState} from 'react';

export const AppContext = React.createContext({});

export default function AppContextProvider({children}) {
  const [appName, setAppName] = useState('Note Sharing');

  const contextValues = {
    appName,
    setAppName,
  };

  return (
    <AppContext.Provider value={contextValues}>{children}</AppContext.Provider>
  );
}
