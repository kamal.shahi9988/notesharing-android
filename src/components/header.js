import React, {useContext} from 'react';
import {useNavigation} from '@react-navigation/native';
import {Button, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import colors from '../config/constant';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {AppContext} from '../contexts/appContext';

const Header = () => {
  const {navigate} = useNavigation();
  const {appName} = useContext(AppContext);

  function gotoNextPage() {
    navigate('LoginScreen', {});
  }

  return (
    <View
      style={{
        // height: 52,
        paddingVertical: 12,
        paddingHorizontal: 15,
        flexDirection: 'row',
        backgroundColor: colors.brand,
        alignItems: 'center',
        justifyContent: 'space-between',
      }}>
      <Text style={{fontWeight: 'bold', color: 'white', fontSize: 20}}>
        {appName}
      </Text>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-end',
        }}>
        <TouchableOpacity style={{marginRight: 15}}>
          <MaterialIcons size={28} color={'white'} name="search" />
        </TouchableOpacity>
        <TouchableOpacity activeOpacity={0.5}>
          <MaterialIcons
            size={27}
            color={'white'}
            name="notifications-active"
          />
          <Badge />
        </TouchableOpacity>
      </View>
    </View>
  );
};

function Badge() {
  return (
    <View style={styles.badge_wrapper}>
      <Text style={{color: 'white', fontSize: 10, fontWeight: 'bold'}}>10</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  badge_wrapper: {
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    width: 20,
    height: 20,
    borderRadius: 50,
    position: 'absolute',
    right: 12,
    top: -5,
  },
});

export default Header;
