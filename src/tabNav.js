import * as React from 'react';
import {View} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Header from './components/header';
import HomeScreen from './screens/homeScreen';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import colors from './config/constant';

const Tab = createBottomTabNavigator();

export default function TabNavigator() {
  return (
    <View style={{flex: 1}}>
      <Header />
      <Tab.Navigator
        screenOptions={{
          headerShown: false,
          tabBarLabelStyle: {
            fontSize: 12,
          },
          tabBarActiveTintColor: colors.brand,
          tabBarInactiveTintColor: 'black',
        }}>
        <Tab.Screen
          options={{
            tabBarLabel: 'Notes',
            tabBarIcon: ({color, size}) => {
              return (
                <MaterialIcons color={color} size={23} name="sticky-note-2" />
              );
            },
          }}
          name="Notes"
          component={HomeScreen}
        />
        <Tab.Screen
          options={{
            tabBarLabel: 'Syllabus',
            tabBarIcon: ({color, size}) => {
              return (
                <MaterialIcons color={color} size={23} name="event-note" />
              );
            },
          }}
          name="Syllabus"
          component={HomeScreen}
        />
        <Tab.Screen
          options={{
            tabBarLabel: 'Questions',
            tabBarIcon: ({color, size}) => {
              return (
                <MaterialIcons color={color} size={23} name="question-answer" />
              );
            },
          }}
          name="Questions"
          component={HomeScreen}
        />
        <Tab.Screen
          options={{
            tabBarLabel: 'Accounts',
            tabBarIcon: ({color, size}) => {
              return (
                <MaterialIcons color={color} size={23} name="account-circle" />
              );
            },
          }}
          name="Accounts"
          component={HomeScreen}
        />
      </Tab.Navigator>
    </View>
  );
}
