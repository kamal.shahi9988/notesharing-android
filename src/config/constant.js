const colors = {
  brand: 'purple',
  grayLight: '#e0e0e0',
  white: 'white',
  orange: 'orange',
  green: 'green',
  black: 'black',
};

export default colors;
