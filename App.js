/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';

import type {Node} from 'react';

import {SafeAreaView, ScrollView, StatusBar, Text} from 'react-native';
import StackNavigation from './src';
import 'react-native-gesture-handler';
import colors from './src/config/constant';
import AppContextProvider from './src/contexts/appContext';

const App: () => Node = () => {
  return (
    <SafeAreaView style={{flex: 1}}>
      <AppContextProvider>
        <StatusBar barStyle={'light-content'} backgroundColor={colors.brand} />
        <StackNavigation />
      </AppContextProvider>
    </SafeAreaView>
  );
};

export default App;
